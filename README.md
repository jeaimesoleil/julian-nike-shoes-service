### Nike Interview Backend Code Challenge [Java]

#### Prerequisite

1. Install OpenJDK 17: https://adoptium.net/
2. Install jenv `brew install jenv`, following https://chamikakasun.medium.com/how-to-manage-multiple-java-version-in-macos-e5421345f6d0 to configure it.
   ```
      // Set java 17 as default jdk version
      jenv global 17
   ```
3. For gitlab-ci, the shell executor is using bash, make sure to switch to `bash`.

#### Play With Code

_The provided code document should contain more details._

This project uses Maven & Spring Boot (Web Starter Pack). So you will need to install `java (> 1.8)` & `maven` on your machine first.
For more information, you can visit the [Spring Boot docs](https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started.html#getting-started.introducing-spring-boot)

1. Clone [this project](https://gitlab.com/hiring_nike_china/fetch-shoe-prices/) and start this nodejs server, following the instructions on this project. The project should run at port 9090
2. You can run `mvn dependency:tree` to list/install the maven dependencies used in this project.
3. To run the server (on port 8081), use this command: `mvn spring-boot:run`

### Retrospect

#### Description of your changes & code improvements
1. Create a new API `/shoe/database` to load all shoes inventory.
2. Add test case for price state recommendation.
3. Create model - Shoe/ShoeProduct, the former is pure shoe, and the latter is shoe ready for sale.

#### If you had more time, what would you add further
1. Booking the shoe and increase/decrease the inventory
2. Swagger

#### What were your doubts, and what were your assumptions for the projects
1. Maximum and minimum price makes me confused. No idea if the random price should be put into the range or not.

#### Any other notes, that are relevant to your task
1. Trying to add CI/CD with runner. There are shared and local runners. No idea which one I should use to align with the interviewer's environment.