package niki.service;

import niki.factory.ShoeProductFactory;
import niki.model.Shoe;
import niki.model.ShoeProduct;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * ShoeProductService
 *
 * @author julian
 * created on 2022/4/16 23:13
 */
@Service
public class ShoeProductService {

    public ShoeProduct getShoeById(Integer id){
        Shoe shoe = ShoeProductFactory.inventory().get(id);
        return ShoeProduct.onSale(shoe, randomPrice(shoe));
    }

    public List<Shoe> shoeDatabase() {
        return ShoeProductFactory.inventory().values().stream().toList();
    }

    private Float randomPrice(Shoe shoe) {
        float min = 10.0f;
        float max = 500.0f;
        Random random = new Random();
        return random.nextFloat() * (max - min) + min;
    }
}
