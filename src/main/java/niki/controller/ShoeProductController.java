package niki.controller;

import niki.model.Shoe;
import niki.model.ShoeProduct;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import niki.service.ShoeProductService;

import java.util.List;

/**
 * ShoeProductController
 *
 * @author julian
 * created on 2022/4/16 23:13
 */
@RestController
public class ShoeProductController {

    private final ShoeProductService shoeProductService;

    public ShoeProductController(ShoeProductService shoeProductService) {
        this.shoeProductService = shoeProductService;
    }

    @GetMapping("/api/shoe-price/{id}")
    @ResponseBody
    public ShoeProduct getShoePrice(@PathVariable Integer id) {
        return shoeProductService.getShoeById(id);
    }

    @GetMapping("/api/shoe/database")
    @ResponseBody
    public List<Shoe> shoeDatabase() {
        return shoeProductService.shoeDatabase();
    }
}
