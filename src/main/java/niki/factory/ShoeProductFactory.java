package niki.factory;

import niki.model.Shoe;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Factory class to supply shoes
 *
 * @author julian
 * created on 2022/4/16 22:36
 */
public final class ShoeProductFactory {

    private ShoeProductFactory() {}

    /**
     * Shoe Inventory
     * @return shoe inventory
     */
    public static Map<Integer, Shoe> inventory() {
        return Stream.of(
            produceShoe(1, "Nike Air Max 95 SE", 120.0f, 150.0f),
            produceShoe(2, "Nike Air Max 97 SE", 5.0f, 150.0f),
            produceShoe(3, "Nike Air Max Pre-Day", 120.0f, 160.0f),
            produceShoe(4, "Nike Air Max 270", 100.0f, 130.0f),
            produceShoe(5, "Nike Renew Ride 3", 180.0f, 200.0f),
            produceShoe(6, "Nike Air Max 90", 120.0f, 150.0f)
        ).collect(Collectors.toConcurrentMap(Shoe::getShoeId, shoe -> shoe));
    }

    private static Shoe produceShoe(Integer id,
                        String model,
                        Float minimumPrice,
                        Float maximumPrice) {
        return new Shoe(id, model, minimumPrice, maximumPrice);
    }
}
