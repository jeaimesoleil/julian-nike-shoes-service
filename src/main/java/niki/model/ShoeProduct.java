package niki.model;

/**
 * Shoe product which is ready to be one sale
 *
 * @author julian
 * created on 2022/4/16 23:09
 */
public class ShoeProduct {

    // Shoe
    private final Shoe shoe;

    // Product property
    private Integer shoeId;
    private String shoeModel;
    private Float shoePrice;
    private Float discountPrice;
    private String state;


    public ShoeProduct(Shoe shoe, Float price) {
        this.shoe = shoe;
        this.shoeId = shoe.getShoeId();
        this.shoeModel = shoe.getModel();
        this.shoePrice = price;
        this.discountPrice = price * 0.4f;
        this.state = calcState();
    }

    public static ShoeProduct onSale(Shoe shoe, Float price) {
        return new ShoeProduct(shoe, price);
    }

    public Integer getShoeId() {
        return shoeId;
    }

    public void setShoeId(Integer shoeId) {
        this.shoeId = shoeId;
    }

    public String getShoeModel() {
        return shoeModel;
    }

    public void setShoeModel(String shoeModel) {
        this.shoeModel = shoeModel;
    }

    public Float getShoePrice() {
        return shoePrice;
    }

    public void setShoePrice(Float shoePrice) {
        this.shoePrice = shoePrice;
    }

    public Float getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(Float discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    protected String calcState() {
        if (discountPrice > this.shoe.getMaximumPrice()) {
            return "Can wait for discount";
        }

        if (discountPrice < this.shoe.getMinimumPrice()) {
            return "Best time to buy!";
        }

        return "Moderate state, can buy now!";
    }
}
