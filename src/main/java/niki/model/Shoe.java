package niki.model;

/**
 * Model of product of nike shoes in inventory
 *
 * @author julian
 * created on 2022/4/16 22:33
 */
public class Shoe {

    private Integer shoeId;

    private String model;

    private Float minimumPrice;

    private Float maximumPrice;

    public Shoe(Integer id, String shoeModel, Float minimumPrice, Float maximumPrice) {
        this.shoeId = id;
        this.model = shoeModel;
        this.minimumPrice = minimumPrice;
        this.maximumPrice = maximumPrice;
    }

    public Integer getShoeId() {
        return shoeId;
    }

    public void setShoeId(Integer shoeId) {
        this.shoeId = shoeId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Float getMinimumPrice() {
        return minimumPrice;
    }

    public void setMinimumPrice(Float minimumPrice) {
        this.minimumPrice = minimumPrice;
    }

    public Float getMaximumPrice() {
        return maximumPrice;
    }

    public void setMaximumPrice(Float maximumPrice) {
        this.maximumPrice = maximumPrice;
    }
}
