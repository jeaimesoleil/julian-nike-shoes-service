package niki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NikeBackendApp {

    public static void main(String[] args) {
        SpringApplication.run(NikeBackendApp.class, args);
    }
}
