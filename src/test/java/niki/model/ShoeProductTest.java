package niki.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShoeProductTest {

    @Test
    void calcState() {
        Shoe bestShoe = new Shoe(1, "Kobe Legends", 100f, 500f);
        ShoeProduct bestProduct = ShoeProduct.onSale(bestShoe, 200f);
        String bestState = bestProduct.calcState();
        assertEquals("Best time to buy!", bestState);

        Shoe canWaitDiscountShoe = new Shoe(1, "Kobe Legends", 100f, 500f);
        ShoeProduct canWaitDiscountProduct = ShoeProduct.onSale(canWaitDiscountShoe, 2000f);
        String canWaitDiscountState = canWaitDiscountProduct.calcState();
        assertEquals("Can wait for discount", canWaitDiscountState);

        Shoe moderateShoe = new Shoe(1, "Kobe Legends", 100f, 500f);
        ShoeProduct moderateProduct = ShoeProduct.onSale(moderateShoe, 500f);
        String moderateState = moderateProduct.calcState();
        assertEquals("Moderate state, can buy now!", moderateState);
    }
}